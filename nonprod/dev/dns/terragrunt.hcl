# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.

terraform {
  source = "git::https://github.com/clouddrove/terraform-aws-route53-record.git"
}

# These are the variables we have to pass in to use the module specified in the terraform source configuration above

inputs = {
  zone_id         = "........."
  name            = "example"
  type            = "A"
  allow_overwrite = true
  alias = {
    name                   = dependency.nlb.outputs.this_lb_dns_name
    zone_id                = dependency.nlb.outputs.this_lb_zone_id
    evaluate_target_health = false
  }
}

## Nothing to change below this line

# Dependecies from other configs
dependency "nlb" {
  config_path = "../networking/nlb"

  mock_outputs = {
    this_lb_dns_name = "lb-dns-name-mock"
    this_lb_zone_id  = "lb-zone-id-mock"
  }
}

# Automatically load higher-level variables

locals {
  env_vars     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  # Extract out common variables for reuse
  env          = local.env_vars.locals.env
  account_name = local.account_vars.locals.account_name
  key_name     = local.env_vars.locals.key_name
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}
