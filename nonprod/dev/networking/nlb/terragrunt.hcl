# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.

terraform {
  source = "git::https://gitlab.com/aosta/ops/aws/alb.git"
}

# These are the variables we have to pass in to use the module specified in the terraform source configuration above

inputs = {
  name               = "${local.account_name}-${local.env}-nlb"
  vpc_id             = dependency.vpc.outputs.vpc_id
  subnets            = dependency.vpc.outputs.public_subnets
  load_balancer_type = "network"

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "TCP"
      target_group_index = 0
    }
  ]

  target_groups = [
    {
      name_prefix      = "tg-"
      backend_protocol = "TCP"
      backend_port     = 80
      target_type      = "instance"
    }
  ]
}

## Nothing to change below this line

# Dependecies from other configs

dependency "vpc" {
  config_path = "../vpc"

  mock_outputs = {
    vpc_id         = "vpc-id-mock"
    public_subnets = ["public-subnets-mock"]
  }
}

# Automatically load higher-level variables

locals {
  env_vars     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  # Extract out common variables for reuse
  env          = local.env_vars.locals.env
  account_name = local.account_vars.locals.account_name
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}
