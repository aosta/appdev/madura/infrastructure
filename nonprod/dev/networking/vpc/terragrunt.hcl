# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.

terraform {
  source = "git::https://gitlab.com/aosta/ops/aws/vpc.git"
}

# These are the variables we have to pass in to use the module specified in the terraform source configuration above

inputs = {
  name = "${local.account_name}-${local.env}-vpc"
  cidr = "10.0.0.0/16"

  azs = [
    "ap-southeast-2a",
    "ap-southeast-2b",
    "ap-southeast-2c"
  ]

  private_subnets = [
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24"
  ]

  public_subnets = [
    "10.0.101.0/24",
    "10.0.102.0/24",
    "10.0.103.0/24"
  ]

  enable_nat_gateway     = true
  single_nat_gateway     = false
  one_nat_gateway_per_az = true
}

## Nothing to change below this line

# Automatically load higher-level variables

locals {
  env_vars     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  # Extract out common variables for reuse
  env          = local.env_vars.locals.env
  account_name = local.account_vars.locals.account_name
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}
