#!/bin/bash

aws configure set default.region ap-southeast-2


tee /var/www/html/.env <<EOF
UNSPLASH_ACCESS_KEY="$(aws --region ap-southeast-2 ssm get-parameter --name "/devops/poc/unsplash-search/development/UNSPLASH_ACCESS_KEY" --with-decryption | jq -r .Parameter.Value)"
UNSPLASH_SECRET_KEY="$(aws --region ap-southeast-2 ssm get-parameter --name "/devops/poc/unsplash-search/development/UNSPLASH_SECRET_KEY" --with-decryption | jq -r .Parameter.Value)"
APP_NAME="$(aws --region ap-southeast-2 ssm get-parameter --name "/devops/poc/unsplash-search/development/APP_NAME" | jq -r .Parameter.Value)"
APP_ENV="$(aws --region ap-southeast-2 ssm get-parameter --name "/devops/poc/unsplash-search/development/APP_ENV" | jq -r .Parameter.Value)"
APP_KEY="$(aws --region ap-southeast-2 ssm get-parameter --name "/devops/poc/unsplash-search/development/APP_KEY" --with-decryption | jq -r .Parameter.Value)"
APP_URL="$(aws --region ap-southeast-2 ssm get-parameter --name "/devops/poc/unsplash-search/development/APP_URL" | jq -r .Parameter.Value | base64 -d)"
APP_DEBUG="$(aws --region ap-southeast-2 ssm get-parameter --name "/devops/poc/unsplash-search/development/APP_DEBUG" | jq -r .Parameter.Value)"
EOF
