# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.

terraform {
  source = "git::https://gitlab.com/aosta/ops/aws/ec2.git"
}

# These are the variables we have to pass in to use the module specified in the terraform source configuration above

inputs = {
  name                        = "${local.account_name}-${local.env}-webserver"
  ami                         = "ami-0c8a177b05fe70159"
  instance_type               = "t3.medium"
  associate_public_ip_address = false
  vpc_security_group_ids      = [dependency.security_group.outputs.this_security_group_id]
  subnet_ids                  = dependency.vpc.outputs.private_subnets
  lb_target_group_arn         = dependency.nlb.outputs.target_group_arns
  lb_target_group_port        = "80"
  user_data                   = file("user_data.sh")
  key_name                    = local.key_name
  iam_instance_profile        = "AmazonSSMRoleForInstancesQuickSetup"
}

## Nothing to change below this line

# Dependecies from other configs

dependency "vpc" {
  config_path = "../../networking/vpc"

  mock_outputs = {
    private_subnets = ["private-subnets-mock"]
  }
}

dependency "security_group" {
  config_path = "../../networking/security-groups/webserver"

  mock_outputs = {
    this_security_group_id = "security-group-id-mock"
  }
}

dependency "nlb" {
  config_path = "../../networking/nlb"

  mock_outputs = {
    target_group_arns = "target-group-arn-mock"
  }
}

# Automatically load higher-level variables

locals {
  env_vars     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  # Extract out common variables for reuse
  env          = local.env_vars.locals.env
  account_name = local.account_vars.locals.account_name
  key_name     = local.env_vars.locals.key_name
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}
