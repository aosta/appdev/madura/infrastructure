# Set account-wide variables. These are automatically pulled in to configure the remote state bucket in the root
# terragrunt.hcl configuration.

locals {
  account_name   = "nonprod"
  aws_account_id = "031397896625"
  aws_region     = "ap-southeast-2"
}
